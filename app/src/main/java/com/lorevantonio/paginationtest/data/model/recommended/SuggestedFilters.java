package com.lorevantonio.paginationtest.model.recommended;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SuggestedFilters{

	@SerializedName("header")
	private String header;

	@SerializedName("filters")
	private List<FiltersItem> filters;

	public void setHeader(String header){
		this.header = header;
	}

	public String getHeader(){
		return header;
	}

	public void setFilters(List<FiltersItem> filters){
		this.filters = filters;
	}

	public List<FiltersItem> getFilters(){
		return filters;
	}

	@Override
 	public String toString(){
		return 
			"SuggestedFilters{" + 
			"header = '" + header + '\'' + 
			",filters = '" + filters + '\'' + 
			"}";
		}
}