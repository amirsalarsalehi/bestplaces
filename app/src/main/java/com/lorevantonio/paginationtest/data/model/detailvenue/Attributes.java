package com.lorevantonio.paginationtest.model.detailvenue;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Attributes{

	@SerializedName("groups")
	private List<Object> groups;

	public void setGroups(List<Object> groups){
		this.groups = groups;
	}

	public List<Object> getGroups(){
		return groups;
	}

	@Override
 	public String toString(){
		return 
			"Attributes{" + 
			"groups = '" + groups + '\'' + 
			"}";
		}
}