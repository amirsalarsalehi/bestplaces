package com.lorevantonio.paginationtest.model.detailvenue;

import com.google.gson.annotations.SerializedName;

public class Colors{

	@SerializedName("highlightColor")
	private HighlightColor highlightColor;

	@SerializedName("algoVersion")
	private int algoVersion;

	@SerializedName("highlightTextColor")
	private HighlightTextColor highlightTextColor;

	public void setHighlightColor(HighlightColor highlightColor){
		this.highlightColor = highlightColor;
	}

	public HighlightColor getHighlightColor(){
		return highlightColor;
	}

	public void setAlgoVersion(int algoVersion){
		this.algoVersion = algoVersion;
	}

	public int getAlgoVersion(){
		return algoVersion;
	}

	public void setHighlightTextColor(HighlightTextColor highlightTextColor){
		this.highlightTextColor = highlightTextColor;
	}

	public HighlightTextColor getHighlightTextColor(){
		return highlightTextColor;
	}

	@Override
 	public String toString(){
		return 
			"Colors{" + 
			"highlightColor = '" + highlightColor + '\'' + 
			",algoVersion = '" + algoVersion + '\'' + 
			",highlightTextColor = '" + highlightTextColor + '\'' + 
			"}";
		}
}