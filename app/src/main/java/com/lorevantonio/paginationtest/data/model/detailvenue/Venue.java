package com.lorevantonio.paginationtest.model.detailvenue;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Venue{

	@SerializedName("reasons")
	private Reasons reasons;

	@SerializedName("specials")
	private Specials specials;

	@SerializedName("dislike")
	private boolean dislike;

	@SerializedName("shortUrl")
	private String shortUrl;

	@SerializedName("rating")
	private double rating;

	@SerializedName("photos")
	private Photos photos;

	@SerializedName("tips")
	private Tips tips;

	@SerializedName("colors")
	private Colors colors;

	@SerializedName("hereNow")
	private HereNow hereNow;

	@SerializedName("createdAt")
	private int createdAt;

	@SerializedName("stats")
	private Stats stats;

	@SerializedName("seasonalHours")
	private List<Object> seasonalHours;

	@SerializedName("contact")
	private Contact contact;

	@SerializedName("ratingColor")
	private String ratingColor;

	@SerializedName("id")
	private String id;

	@SerializedName("categories")
	private List<CategoriesItem> categories;

	@SerializedName("ok")
	private boolean ok;

	@SerializedName("allowMenuUrlEdit")
	private boolean allowMenuUrlEdit;

	@SerializedName("likes")
	private Likes likes;

	@SerializedName("canonicalUrl")
	private String canonicalUrl;

	@SerializedName("verified")
	private boolean verified;

	@SerializedName("timeZone")
	private String timeZone;

	@SerializedName("beenHere")
	private BeenHere beenHere;

	@SerializedName("bestPhoto")
	private BestPhoto bestPhoto;

	@SerializedName("ratingSignals")
	private int ratingSignals;

	@SerializedName("listed")
	private Listed listed;

	@SerializedName("name")
	private String name;

	@SerializedName("location")
	private Location location;

	@SerializedName("attributes")
	private Attributes attributes;

	@SerializedName("pageUpdates")
	private PageUpdates pageUpdates;

	@SerializedName("inbox")
	private Inbox inbox;

	public void setReasons(Reasons reasons){
		this.reasons = reasons;
	}

	public Reasons getReasons(){
		return reasons;
	}

	public void setSpecials(Specials specials){
		this.specials = specials;
	}

	public Specials getSpecials(){
		return specials;
	}

	public void setDislike(boolean dislike){
		this.dislike = dislike;
	}

	public boolean isDislike(){
		return dislike;
	}

	public void setShortUrl(String shortUrl){
		this.shortUrl = shortUrl;
	}

	public String getShortUrl(){
		return shortUrl;
	}

	public void setRating(double rating){
		this.rating = rating;
	}

	public double getRating(){
		return rating;
	}

	public void setPhotos(Photos photos){
		this.photos = photos;
	}

	public Photos getPhotos(){
		return photos;
	}

	public void setTips(Tips tips){
		this.tips = tips;
	}

	public Tips getTips(){
		return tips;
	}

	public void setColors(Colors colors){
		this.colors = colors;
	}

	public Colors getColors(){
		return colors;
	}

	public void setHereNow(HereNow hereNow){
		this.hereNow = hereNow;
	}

	public HereNow getHereNow(){
		return hereNow;
	}

	public void setCreatedAt(int createdAt){
		this.createdAt = createdAt;
	}

	public int getCreatedAt(){
		return createdAt;
	}

	public void setStats(Stats stats){
		this.stats = stats;
	}

	public Stats getStats(){
		return stats;
	}

	public void setSeasonalHours(List<Object> seasonalHours){
		this.seasonalHours = seasonalHours;
	}

	public List<Object> getSeasonalHours(){
		return seasonalHours;
	}

	public void setContact(Contact contact){
		this.contact = contact;
	}

	public Contact getContact(){
		return contact;
	}

	public void setRatingColor(String ratingColor){
		this.ratingColor = ratingColor;
	}

	public String getRatingColor(){
		return ratingColor;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCategories(List<CategoriesItem> categories){
		this.categories = categories;
	}

	public List<CategoriesItem> getCategories(){
		return categories;
	}

	public void setOk(boolean ok){
		this.ok = ok;
	}

	public boolean isOk(){
		return ok;
	}

	public void setAllowMenuUrlEdit(boolean allowMenuUrlEdit){
		this.allowMenuUrlEdit = allowMenuUrlEdit;
	}

	public boolean isAllowMenuUrlEdit(){
		return allowMenuUrlEdit;
	}

	public void setLikes(Likes likes){
		this.likes = likes;
	}

	public Likes getLikes(){
		return likes;
	}

	public void setCanonicalUrl(String canonicalUrl){
		this.canonicalUrl = canonicalUrl;
	}

	public String getCanonicalUrl(){
		return canonicalUrl;
	}

	public void setVerified(boolean verified){
		this.verified = verified;
	}

	public boolean isVerified(){
		return verified;
	}

	public void setTimeZone(String timeZone){
		this.timeZone = timeZone;
	}

	public String getTimeZone(){
		return timeZone;
	}

	public void setBeenHere(BeenHere beenHere){
		this.beenHere = beenHere;
	}

	public BeenHere getBeenHere(){
		return beenHere;
	}

	public void setBestPhoto(BestPhoto bestPhoto){
		this.bestPhoto = bestPhoto;
	}

	public BestPhoto getBestPhoto(){
		return bestPhoto;
	}

	public void setRatingSignals(int ratingSignals){
		this.ratingSignals = ratingSignals;
	}

	public int getRatingSignals(){
		return ratingSignals;
	}

	public void setListed(Listed listed){
		this.listed = listed;
	}

	public Listed getListed(){
		return listed;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setLocation(Location location){
		this.location = location;
	}

	public Location getLocation(){
		return location;
	}

	public void setAttributes(Attributes attributes){
		this.attributes = attributes;
	}

	public Attributes getAttributes(){
		return attributes;
	}

	public void setPageUpdates(PageUpdates pageUpdates){
		this.pageUpdates = pageUpdates;
	}

	public PageUpdates getPageUpdates(){
		return pageUpdates;
	}

	public void setInbox(Inbox inbox){
		this.inbox = inbox;
	}

	public Inbox getInbox(){
		return inbox;
	}

	@Override
 	public String toString(){
		return 
			"Venue{" + 
			"reasons = '" + reasons + '\'' + 
			",specials = '" + specials + '\'' + 
			",dislike = '" + dislike + '\'' + 
			",shortUrl = '" + shortUrl + '\'' + 
			",rating = '" + rating + '\'' + 
			",photos = '" + photos + '\'' + 
			",tips = '" + tips + '\'' + 
			",colors = '" + colors + '\'' + 
			",hereNow = '" + hereNow + '\'' + 
			",createdAt = '" + createdAt + '\'' + 
			",stats = '" + stats + '\'' + 
			",seasonalHours = '" + seasonalHours + '\'' + 
			",contact = '" + contact + '\'' + 
			",ratingColor = '" + ratingColor + '\'' + 
			",id = '" + id + '\'' + 
			",categories = '" + categories + '\'' + 
			",ok = '" + ok + '\'' + 
			",allowMenuUrlEdit = '" + allowMenuUrlEdit + '\'' + 
			",likes = '" + likes + '\'' + 
			",canonicalUrl = '" + canonicalUrl + '\'' + 
			",verified = '" + verified + '\'' + 
			",timeZone = '" + timeZone + '\'' + 
			",beenHere = '" + beenHere + '\'' + 
			",bestPhoto = '" + bestPhoto + '\'' + 
			",ratingSignals = '" + ratingSignals + '\'' + 
			",listed = '" + listed + '\'' + 
			",name = '" + name + '\'' + 
			",location = '" + location + '\'' + 
			",attributes = '" + attributes + '\'' + 
			",pageUpdates = '" + pageUpdates + '\'' + 
			",inbox = '" + inbox + '\'' + 
			"}";
		}
}