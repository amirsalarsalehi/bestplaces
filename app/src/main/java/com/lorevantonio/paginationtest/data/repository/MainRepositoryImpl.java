package com.lorevantonio.paginationtest.repository;

import com.lorevantonio.paginationtest.model.detailvenue.VenueDetailResponse;
import com.lorevantonio.paginationtest.model.recommended.BaseRecommendedResponse;
import com.lorevantonio.paginationtest.network.MainApi;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Retrofit;

public class MainRepositoryImpl implements MainRepository {

    @Inject
    Retrofit retrofit;

    @Inject
    public MainRepositoryImpl() {

    }

    @Override
    public Single<BaseRecommendedResponse> getRecommendedRep(String clientId, String clientSecret, String v, String ll, Integer offset) {
        return retrofit.create(MainApi.class).getRecommended(clientId, clientSecret, v, ll, offset);
    }

    @Override
    public Single<VenueDetailResponse> getVenueDetail(String id, String clientId, String clientSecret, String v) {
        return retrofit.create(MainApi.class).getVenueDetail(id, clientId, clientSecret, v);
    }

}
