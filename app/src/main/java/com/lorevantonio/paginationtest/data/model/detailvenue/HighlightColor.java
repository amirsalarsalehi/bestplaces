package com.lorevantonio.paginationtest.model.detailvenue;

import com.google.gson.annotations.SerializedName;

public class HighlightColor{

	@SerializedName("photoId")
	private String photoId;

	@SerializedName("value")
	private int value;

	public void setPhotoId(String photoId){
		this.photoId = photoId;
	}

	public String getPhotoId(){
		return photoId;
	}

	public void setValue(int value){
		this.value = value;
	}

	public int getValue(){
		return value;
	}

	@Override
 	public String toString(){
		return 
			"HighlightColor{" + 
			"photoId = '" + photoId + '\'' + 
			",value = '" + value + '\'' + 
			"}";
		}
}