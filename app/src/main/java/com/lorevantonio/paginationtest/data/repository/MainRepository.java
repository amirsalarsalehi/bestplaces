package com.lorevantonio.paginationtest.repository;

import com.lorevantonio.paginationtest.model.detailvenue.VenueDetailResponse;
import com.lorevantonio.paginationtest.model.recommended.BaseRecommendedResponse;
import com.lorevantonio.paginationtest.network.MainApi;

import io.reactivex.Single;

public interface MainRepository {

    Single<BaseRecommendedResponse> getRecommendedRep(
            String clientId,
            String clientSecret,
            String v,
            String ll,
            Integer offset
    );

    Single<VenueDetailResponse> getVenueDetail(
            String id,
            String clientId,
            String clientSecret,
            String v
    );

}
