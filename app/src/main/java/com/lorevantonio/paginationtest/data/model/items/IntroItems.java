package com.lorevantonio.paginationtest.model.items;

public class IntroItems {

    String title, desc;
    int imageId;

    public IntroItems(String title, String desc, int imageId) {
        this.title = title;
        this.desc = desc;
        this.imageId = imageId;
    }

    public String getTitle() {
        return title;
    }


    public String getDesc() {
        return desc;
    }


    public int getImageId() {
        return imageId;
    }

}
