package com.lorevantonio.paginationtest.network;

import com.lorevantonio.paginationtest.model.detailvenue.VenueDetailResponse;
import com.lorevantonio.paginationtest.model.recommended.BaseRecommendedResponse;

import io.reactivex.Flowable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MainApi {

    @GET("explore")
    Single<BaseRecommendedResponse> getRecommended(
            @Query("client_id") String clientId,
            @Query("client_secret") String clientSecret,
            @Query("v") String v,
            @Query("ll") String ll,
            @Query("offset") int offset
    );

    @GET("{id}")
    Single<VenueDetailResponse> getVenueDetail(
            @Path("id") String venueId,
            @Query("client_id") String clientId,
            @Query("client_secret") String clientSecret,
            @Query("v") String v
    );
}
