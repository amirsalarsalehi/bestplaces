package com.lorevantonio.paginationtest.model.detailvenue;

import com.google.gson.annotations.SerializedName;

public class Stats{

	@SerializedName("tipCount")
	private int tipCount;

	public void setTipCount(int tipCount){
		this.tipCount = tipCount;
	}

	public int getTipCount(){
		return tipCount;
	}

	@Override
 	public String toString(){
		return 
			"Stats{" + 
			"tipCount = '" + tipCount + '\'' + 
			"}";
		}
}