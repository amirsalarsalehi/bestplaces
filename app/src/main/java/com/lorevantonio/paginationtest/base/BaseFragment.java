package com.lorevantonio.paginationtest;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.lorevantonio.paginationtest.view.fragment.main.MainViewModel;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import retrofit2.Retrofit;

public abstract class BaseFragment extends DaggerFragment {

    private static final String TAG = "BaseFragment";

    public abstract void viewModel();

    public abstract View init(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    public abstract void attachLiveData();

    private View viewRoot;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (viewRoot == null) {
            viewRoot = init(inflater, container, savedInstanceState);
            viewModel();
            attachLiveData();
            ((AppCompatActivity) requireActivity()).getSupportActionBar().show();

            Log.d(TAG, "onCreateView: is null");
        } else
            Log.d(TAG, "onCreateView: is not null");

        return viewRoot;
    }
}
