package com.lorevantonio.paginationtest.di;

import com.lorevantonio.paginationtest.repository.MainRepository;
import com.lorevantonio.paginationtest.repository.MainRepositoryImpl;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class RepositoryBuilderModule {

    @Binds
    abstract MainRepository bindRepository(MainRepositoryImpl mainRepository);

}
