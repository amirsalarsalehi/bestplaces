package com.lorevantonio.paginationtest.di.main;

import androidx.lifecycle.ViewModel;

import com.lorevantonio.paginationtest.di.ViewModelKey;
import com.lorevantonio.paginationtest.view.fragment.main.MainViewModel;
import com.lorevantonio.paginationtest.view.fragment.result.ResultViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class MainViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel mainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ResultViewModel.class)
    abstract ViewModel bindResultViewModel(ResultViewModel resultViewModel);

}
