package com.lorevantonio.paginationtest.di;

import androidx.lifecycle.ViewModelProvider;

import com.lorevantonio.paginationtest.viewmodel.ViewModelProviderFactory;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ViewModelProviderFactoryModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelProviderFactory(ViewModelProviderFactory providerFactory);

}
