package com.lorevantonio.paginationtest.di.main;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.lorevantonio.paginationtest.R;
import com.lorevantonio.paginationtest.view.activities.MainActivity;

import dagger.Module;
import dagger.Provides;
import dagger.android.support.DaggerAppCompatActivity;

@Module
public class MainModule {

}
