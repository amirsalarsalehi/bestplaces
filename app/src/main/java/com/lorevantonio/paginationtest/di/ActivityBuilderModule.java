package com.lorevantonio.paginationtest.di;

import com.lorevantonio.paginationtest.di.main.MainModule;
import com.lorevantonio.paginationtest.di.main.MainViewModelModule;
import com.lorevantonio.paginationtest.view.activities.MainActivity;
import com.lorevantonio.paginationtest.di.main.MainFragmentBuilderModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(
            modules = {
                    MainFragmentBuilderModule.class,
                    MainModule.class,
                    MainViewModelModule.class
            }
    )
    abstract MainActivity contributeMainActivity();

}
