package com.lorevantonio.paginationtest.di.main;

import com.lorevantonio.paginationtest.view.fragment.main.MainFragment;
import com.lorevantonio.paginationtest.view.fragment.result.ResultFragment;
import com.lorevantonio.paginationtest.view.fragment.splash.GetStartedFragment;
import com.lorevantonio.paginationtest.view.fragment.splash.IntroductionFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainFragmentBuilderModule {

    @ContributesAndroidInjector
    abstract MainFragment contributeMainFragment();

    @ContributesAndroidInjector()
    abstract IntroductionFragment contributeIntroductionFragment();

    @ContributesAndroidInjector
    abstract GetStartedFragment contributeGetStartedFragment();

    @ContributesAndroidInjector
    abstract ResultFragment contributeResultFragment();


}
