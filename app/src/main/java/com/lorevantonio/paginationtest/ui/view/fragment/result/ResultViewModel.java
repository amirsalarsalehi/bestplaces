package com.lorevantonio.paginationtest.view.fragment.result;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MutableLiveData;

import com.lorevantonio.paginationtest.model.detailvenue.Meta;
import com.lorevantonio.paginationtest.model.detailvenue.Venue;
import com.lorevantonio.paginationtest.model.detailvenue.VenueDetailResponse;
import com.lorevantonio.paginationtest.network.MainApi;
import com.lorevantonio.paginationtest.repository.MainRepository;
import com.lorevantonio.paginationtest.repository.MainRepositoryImpl;
import com.lorevantonio.paginationtest.util.ConstantVariables;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class ResultViewModel extends AndroidViewModel {

    private static final String TAG = "ResultViewModel";

    private CompositeDisposable disposable = new CompositeDisposable();

    @Inject
    MainRepositoryImpl mainRepository;


    MutableLiveData<String> idLiveData = new MutableLiveData<>();
    MutableLiveData<VenueDetailResponse> venueDetailLiveData = new MutableLiveData<>();

    @Inject
    public ResultViewModel(@NonNull Application application) {
        super(application);
    }

    public void observeVenue() {

        mainRepository.getVenueDetail(
                idLiveData.getValue(),
                ConstantVariables.CLIENT_ID,
                ConstantVariables.CLIENT_SECRET,
                getDateFormatted()
        ).subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<VenueDetailResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onSuccess(VenueDetailResponse venueDetailResponse) {
                        venueDetailLiveData.postValue(venueDetailResponse);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    public String getDateFormatted() {
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        return df.format(new Date());
    }

}
