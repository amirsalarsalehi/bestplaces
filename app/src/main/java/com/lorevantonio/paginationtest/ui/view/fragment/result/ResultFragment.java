package com.lorevantonio.paginationtest.view.fragment.result;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.constants.ScaleTypes;
import com.denzcoskun.imageslider.models.SlideModel;
import com.lorevantonio.paginationtest.BaseFragment;
import com.lorevantonio.paginationtest.R;
import com.lorevantonio.paginationtest.model.detailvenue.VenueDetailResponse;
import com.lorevantonio.paginationtest.viewmodel.ViewModelProviderFactory;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;


public class ResultFragment extends BaseFragment {

    private static final String TAG = "ResultFragment";

    TextView txtVenueName, txtVenueCity, txtVenueRate, txtVenueLike, txtVenueCategoryName;

    @Inject
    ViewModelProviderFactory providerFactory;

    private ImageSlider venueImageSlider;

    private ResultViewModel viewModel;

    public ResultFragment() {

    }

    @Override
    public void viewModel() {
        viewModel = new ViewModelProvider(this, providerFactory).get(ResultViewModel.class);
    }

    @Override
    public View init(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_result, container, false);
    }

    @Override
    public void attachLiveData() {
        viewModel.idLiveData.setValue(getArguments().getString("venueId"));
        viewModel.observeVenue();
        viewModel.venueDetailLiveData.observe(this, new Observer<VenueDetailResponse>() {
            @Override
            public void onChanged(VenueDetailResponse venueDetailResponse) {
                if (venueDetailResponse != null) {
                    txtVenueName.setText(venueDetailResponse.getResponse().getVenue().getName());
                    txtVenueCity.setText(venueDetailResponse.getResponse().getVenue().getLocation().getCity());
                    txtVenueRate.setText(String.valueOf(venueDetailResponse.getResponse().getVenue().getRating()));
                    txtVenueLike.setText(String.valueOf(venueDetailResponse.getResponse().getVenue().getLikes().getCount()));
                    txtVenueCategoryName.setText(venueDetailResponse.getResponse().getVenue().getCategories().get(0).getName());
                    List<SlideModel> list = new ArrayList<>();
                    list.add(new SlideModel(
                            venueDetailResponse.getResponse().getVenue().getBestPhoto().getPrefix() +
                                    venueDetailResponse.getResponse().getVenue().getBestPhoto().getWidth() + "x" +
                                    venueDetailResponse.getResponse().getVenue().getBestPhoto().getHeight() +
                                    venueDetailResponse.getResponse().getVenue().getBestPhoto().getSuffix(), ScaleTypes.CENTER_CROP));
                    venueImageSlider.setImageList(list, ScaleTypes.CENTER_CROP);
                } else {
                    Log.d(TAG, "onChanged: null");
                }
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        txtVenueName = view.findViewById(R.id.txtVenueName);
        txtVenueCity = view.findViewById(R.id.txtVenueCity);
        txtVenueRate = view.findViewById(R.id.txtVenueRate);
        txtVenueLike = view.findViewById(R.id.txtVenueLike);
        txtVenueCategoryName = view.findViewById(R.id.txtVenueCategoryName);
        venueImageSlider = view.findViewById(R.id.venueImageSlider);
    }
}