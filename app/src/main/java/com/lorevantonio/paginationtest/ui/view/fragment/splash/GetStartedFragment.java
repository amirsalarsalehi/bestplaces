package com.lorevantonio.paginationtest.view.fragment.splash;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.lorevantonio.paginationtest.BaseFragment;
import com.lorevantonio.paginationtest.R;

import dagger.android.support.DaggerFragment;


public class GetStartedFragment extends BaseFragment implements View.OnClickListener {

    private Animation animation;

    private Button btnGettingStarted;
    private ImageView imgView;

    private NavController navController;



    public GetStartedFragment() {

    }

    @Override
    public void viewModel() {

    }

    @Override
    public View init(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_get_started, container, false);
    }

    @Override
    public void attachLiveData() {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        btnGettingStarted = view.findViewById(R.id.btnGettingStarted);
        navController = Navigation.findNavController(getActivity(), R.id.fr_host);
        imgView = view.findViewById(R.id.imgView);

        animation = AnimationUtils.loadAnimation(getContext(), R.anim.getting_animation);

        btnGettingStarted.setAnimation(animation);
        btnGettingStarted.setOnClickListener(this);

        imgView.setAnimation(animation);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnGettingStarted:
                navController.navigate(R.id.action_getStartedFragment_to_mainFragment);
        }
    }
}