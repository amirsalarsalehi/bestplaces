package com.lorevantonio.paginationtest.view.fragment.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.lorevantonio.paginationtest.BaseFragment;
import com.lorevantonio.paginationtest.R;
import com.lorevantonio.paginationtest.model.recommended.BaseRecommendedResponse;
import com.lorevantonio.paginationtest.model.recommended.ItemsItem;
import com.lorevantonio.paginationtest.util.ConstantVariables;
import com.lorevantonio.paginationtest.util.adapter.RecommendedAdapter;
import com.lorevantonio.paginationtest.util.other.RecyclerViewListenerHelper;
import com.lorevantonio.paginationtest.viewmodel.ViewModelProviderFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

public class MainFragment extends BaseFragment implements OnCompleteListener<Location>, RecommendedAdapter.OnItemRecommendedClickListener {

    private final String TAG = "MainFragment";

    private FusedLocationProviderClient client;

    private MainViewModel viewModel;

    private TextView txtYourGeo, txtYourCountry, txtYourLocality;
    private RecyclerView recommendedRecyclerView;
    private ProgressBar mainProgressBar;
    private LinearLayoutManager layoutManager;
    private RecommendedAdapter adapter;

    private static final int TOTAL_PAGE = 5;
    private int currentPage = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private boolean isLoadingFirstPage = true;

    private NavController navController;

    @Inject
    ViewModelProviderFactory providerFactory;

    public MainFragment() {

    }

    @Override
    public void viewModel() {
        viewModel = new ViewModelProvider(this, providerFactory).get(MainViewModel.class);
    }

    @Override
    public View init(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void attachLiveData() {
        viewModel.responseLiveData.observe(getViewLifecycleOwner(), new Observer<BaseRecommendedResponse>() {
            @Override
            public void onChanged(BaseRecommendedResponse baseRecommendedResponse) {
                if (isLoadingFirstPage) {

                    mainProgressBar.setVisibility(View.GONE);
                    adapter.addAll(baseRecommendedResponse.getResponse().getGroups().get(0).getItems());
                    if (currentPage <= TOTAL_PAGE) {
                        adapter.addLoadingFooter();
                    } else
                        isLastPage = true;

                    Log.d(TAG, "onChanged: == 1");
                } else {
                    adapter.removeLoadingFooter();
                    isLoading = false;

                    adapter.addAll(baseRecommendedResponse.getResponse().getGroups().get(0).getItems());
                    if (currentPage != TOTAL_PAGE) {
                        adapter.addLoadingFooter();
                    } else
                        isLastPage = true;

                    Log.d(TAG, "onChanged: != 1");

                }
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        txtYourGeo = view.findViewById(R.id.txtYourGeo);
        txtYourCountry = view.findViewById(R.id.txtYourCountry);
        txtYourLocality = view.findViewById(R.id.txtYourLocality);
        recommendedRecyclerView = view.findViewById(R.id.recommendedRecyclerView);
        mainProgressBar = view.findViewById(R.id.mainProgressBar);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        adapter = new RecommendedAdapter(getActivity(), this);
        navController = Navigation.findNavController(getActivity(), R.id.fr_host);
        client = new FusedLocationProviderClient(getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        getLocation();
    }

    @SuppressLint("MissingPermission")
    private void getLocation() {
        if (isGranted(Manifest.permission.ACCESS_COARSE_LOCATION) && isGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            client.getLastLocation().addOnCompleteListener(this);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, 1);
            Log.d(TAG, "getLocation: not granted");
        }
    }

    @Override
    public void onComplete(@NonNull Task<Location> task) {
        Location location = task.getResult();
        List<Address> addresses = null;
        if (location != null) {
            try {
                Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                addresses = geocoder.getFromLocation(
                        location.getLatitude(),
                        location.getLongitude(),
                        1
                );
                initRecycler(location, addresses);
            } catch (Exception e) {
                Log.d(TAG, "onComplete: " + e.toString());
            }
        }
    }


    private void initRecycler(final Location currentLocation, final List<Address> addressList) {
        txtYourGeo.setText(String.format("%s,%s", currentLocation.getLatitude(), currentLocation.getLongitude()));
        txtYourCountry.setText(addressList.get(0).getCountryName());
        txtYourLocality.setText(addressList.get(0).getLocality());

        viewModel.llLiveData.setValue(String.format("%s,%s", currentLocation.getLatitude(), currentLocation.getLongitude()));
        viewModel.offsetLiveData.setValue(currentPage);

        recommendedRecyclerView.setLayoutManager(layoutManager);
        recommendedRecyclerView.setAdapter(adapter);

        recommendedRecyclerView.addOnScrollListener(new RecyclerViewListenerHelper(layoutManager) {
            @Override
            public void getMoreItems() {
                currentPage += 1;
                isLoading = true;
                recommendedRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        loadMore(currentLocation);
                    }
                });
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public int totalPageCount() {
                return TOTAL_PAGE;
            }
        });

        recommendedRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                loadFirstPage();
            }
        });

    }

    @Override
    public void onItemClick(int pos) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantVariables.VENUE_ID, adapter.getItem(pos).getVenue().getId());
        Log.d(TAG, "onItemClick: " + adapter.getItem(pos).getVenue().getId());
        navController.navigate(R.id.action_mainFragment_to_resultFragment, bundle);
    }

    @Override
    public void onItemLongClick(int pos) {
        Toast.makeText(getActivity(), "" + pos, Toast.LENGTH_SHORT).show();
    }


    private void loadFirstPage() {
        viewModel.callRemoteGetBaseRecommended();
        Log.d(TAG, "loadFirstPage: " + viewModel.offsetLiveData.getValue());
        this.isLoadingFirstPage = true;
        attachLiveData();
    }

    private synchronized void loadMore(final Location currentLocation) {
        viewModel.setLlValue(String.format("%s,%s", currentLocation.getLatitude(), currentLocation.getLongitude()));
        viewModel.setOffsetLiveData(currentPage);
        viewModel.callRemoteGetBaseRecommended();
        this.isLoadingFirstPage = false;
        attachLiveData();
    }

    private boolean isGranted(String permission) {
        return ActivityCompat.checkSelfPermission(requireActivity(), permission) == PackageManager.PERMISSION_GRANTED;
    }
}

