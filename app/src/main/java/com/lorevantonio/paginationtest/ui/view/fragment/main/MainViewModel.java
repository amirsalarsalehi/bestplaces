package com.lorevantonio.paginationtest.view.fragment.main;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.lorevantonio.paginationtest.model.recommended.BaseRecommendedResponse;
import com.lorevantonio.paginationtest.network.MainApi;
import com.lorevantonio.paginationtest.repository.MainRepository;
import com.lorevantonio.paginationtest.repository.MainRepositoryImpl;
import com.lorevantonio.paginationtest.util.ConstantVariables;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class MainViewModel extends AndroidViewModel {

    private static final String TAG = "MainViewModel";

    private CompositeDisposable disposable = new CompositeDisposable();


    public MutableLiveData<BaseRecommendedResponse> responseLiveData = new MutableLiveData<>();
    public MutableLiveData<Throwable> throwableMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<String> llLiveData = new MutableLiveData<>();
    public MutableLiveData<Integer> offsetLiveData = new MutableLiveData<>();

    @Inject
    MainRepositoryImpl mainRepository;

    @Inject
    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    public void callRemoteGetBaseRecommended() {

        mainRepository.getRecommendedRep(
                ConstantVariables.CLIENT_ID,
                ConstantVariables.CLIENT_SECRET,
                getDateFormatted(),
                llLiveData.getValue(),
                offsetLiveData.getValue())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<BaseRecommendedResponse>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onSuccess(BaseRecommendedResponse baseRecommendedResponse) {
                        responseLiveData.postValue(baseRecommendedResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        throwableMutableLiveData.postValue(e);
                    }

                });
    }

    public String getDateFormatted() {
        DateFormat df = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
        Log.d(TAG, "getDateFormatted: " + df.format(new Date()));
        return df.format(new Date());
    }

    public void setLlValue(String ll) {
        llLiveData.setValue(ll);
    }

    public void setOffsetLiveData(int i) {
        offsetLiveData.setValue(i);
    }

}






