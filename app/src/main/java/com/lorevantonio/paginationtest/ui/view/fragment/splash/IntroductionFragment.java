package com.lorevantonio.paginationtest.view.fragment.splash;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.lorevantonio.paginationtest.BaseFragment;
import com.lorevantonio.paginationtest.R;
import com.lorevantonio.paginationtest.model.items.IntroItems;
import com.lorevantonio.paginationtest.util.adapter.IntroAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

public class IntroductionFragment extends BaseFragment
        implements View.OnClickListener, TabLayout.OnTabSelectedListener {

    private ViewPager viewPagerIntro;
    private TabLayout tabIndicator;
    private AppCompatActivity app;
    private Button btnNext;

    private IntroAdapter adapter;

    private int currentPosition = 0;

    private List<IntroItems> mList;

    private NavController navController;

    public IntroductionFragment() {

    }

    @Override
    public void viewModel() {

    }

    @Override
    public View init(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_intro, container, false);
    }

    @Override
    public void attachLiveData() {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        app = (AppCompatActivity) getActivity();
        app.getSupportActionBar().hide();
        viewPagerIntro = view.findViewById(R.id.viewPagerIntro);
        tabIndicator = view.findViewById(R.id.tabIndicator);
        btnNext = view.findViewById(R.id.btn_next);
        navController = Navigation.findNavController(getActivity(), R.id.fr_host);

        btnNext.setOnClickListener(this);

        mList = new ArrayList<>();
        mList.add(
                new IntroItems("Best experience",
                        "Enjoy our application and have fun",
                        R.drawable.m1));
        mList.add(
                new IntroItems("Fast API",
                        "using best web services",
                        R.drawable.marker));

        mList.add(
                new IntroItems("",
                        "give you best places information",
                        R.drawable.bestpl));

        adapter = new IntroAdapter(getContext(), mList);
        viewPagerIntro.setAdapter(adapter);
        tabIndicator.setupWithViewPager(viewPagerIntro);
        tabIndicator.addOnTabSelectedListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                currentPosition = viewPagerIntro.getCurrentItem();
                if (currentPosition < mList.size()) {
                    currentPosition++;
                    viewPagerIntro.setCurrentItem(currentPosition);
                }
                if (currentPosition == mList.size()) {
                    navController.navigate(R.id.action_introductionFragment_to_getStartedFragment);
                }
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (tab.getPosition() == mList.size() - 1) {
            btnNext.setText("Finish");
        } else {
            btnNext.setText("Next");
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

}
