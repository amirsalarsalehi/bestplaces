package com.lorevantonio.paginationtest.util.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lorevantonio.paginationtest.R;
import com.lorevantonio.paginationtest.model.recommended.ItemsItem;

import java.util.ArrayList;
import java.util.List;

public class RecommendedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int ITEM = 0;
    public static final int LOADING = 1;

    private List<ItemsItem> list;
    private Context context;

    private boolean isLoadingAdded;
    private OnItemRecommendedClickListener listener;

    public RecommendedAdapter(Context context, OnItemRecommendedClickListener listener) {
        this.list = new ArrayList<>();
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case ITEM:
                viewHolder = new RecommendedItemViewHolder(LayoutInflater.from(context).inflate(R.layout.items_recommended, parent, false), listener);
                break;
            case LOADING:
                viewHolder = new LoadingViewHolder(LayoutInflater.from(context).inflate(R.layout.items_loading, parent, false));
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case ITEM:
                ((RecommendedItemViewHolder) holder).bind(list.get(position));
                break;
            case LOADING:
                return;
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == list.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void setList(List<ItemsItem> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public ItemsItem getItem(int pos) {
        return list.get(pos);
    }

    public void add(ItemsItem items) {
        list.add(items);
        notifyItemInserted(list.size() - 1);
    }

    public void addAll(List<ItemsItem> items) {
        for (ItemsItem item : items) {
            add(item);
        }
    }

    public void remove(ItemsItem items) {
        if (list.indexOf(items) > -1) {
            list.remove(list.indexOf(items));
            notifyItemRemoved(list.indexOf(items));
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ItemsItem());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;
        if (list.size() > 0) {
            if (list.get(list.size() - 1) != null) {
                list.remove(list.size() - 1);
                notifyItemRemoved(list.size() - 1);
            }
        }
    }

    public interface OnItemRecommendedClickListener {
        void onItemClick(int pos);

        void onItemLongClick(int pos);
    }

    private class RecommendedItemViewHolder extends RecyclerView.ViewHolder {

        ImageView img;
        TextView txtName, txtCountry, txtCity;

        public RecommendedItemViewHolder(@NonNull View itemView, final OnItemRecommendedClickListener listener) {
            super(itemView);
            img = itemView.findViewById(R.id.items_rec_img);
            txtName = itemView.findViewById(R.id.items_rec_name);
            txtCity = itemView.findViewById(R.id.items_rec_city);
            txtCountry = itemView.findViewById(R.id.items_rec_country);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(getAdapterPosition());
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listener.onItemLongClick(getAdapterPosition());
                    return true;
                }
            });
        }

        public void bind(final ItemsItem item) {

            if (item == null || item.getVenue() == null)
                return;

            if (item.getVenue().getName() != null) {
                txtName.setText(item.getVenue().getName());
                txtCity.setText(item.getVenue().getLocation().getCity());
                txtCountry.setText(item.getVenue().getLocation().getCountry());
                img.setImageResource(R.drawable.m1);
            }
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
