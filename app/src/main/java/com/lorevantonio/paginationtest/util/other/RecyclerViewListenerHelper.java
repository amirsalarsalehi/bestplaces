package com.lorevantonio.paginationtest.util.other;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class RecyclerViewListenerHelper extends RecyclerView.OnScrollListener {

    LinearLayoutManager layoutManager;

    public RecyclerViewListenerHelper(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstItemVisible = layoutManager.findFirstVisibleItemPosition();

        if (!isLoading() && !isLastPage()) {
            if ((visibleItemCount + firstItemVisible) >= totalItemCount - 10 && totalItemCount >= totalPageCount()) {
                getMoreItems();
            }
        }
    }

    public abstract void getMoreItems();

    public abstract boolean isLoading();

    public abstract boolean isLastPage();

    public abstract int totalPageCount();
}
