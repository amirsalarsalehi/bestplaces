package com.lorevantonio.paginationtest.util.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.lorevantonio.paginationtest.R;
import com.lorevantonio.paginationtest.model.items.IntroItems;

import java.util.List;

public class IntroAdapter extends PagerAdapter {

    private Context context;
    private List<IntroItems> introItemsList;

    public IntroAdapter(Context context, List<IntroItems> introItemsList) {
        this.context = context;
        this.introItemsList = introItemsList;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.intro_layout_items, null);

        ImageView imageView = view.findViewById(R.id.imgIntro);
        TextView txtTitle =  view.findViewById(R.id.tvTitleIntro);
        TextView txtDesc =  view.findViewById(R.id.tvDescIntro);

        imageView.setImageResource(introItemsList.get(position).getImageId());
        txtTitle.setText(introItemsList.get(position).getTitle());
        txtDesc.setText(introItemsList.get(position).getDesc());

        container.addView(view);
        return view;

    }

    @Override
    public int getCount() {
        return introItemsList == null ? 0 : introItemsList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
